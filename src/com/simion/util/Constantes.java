package com.simion.util;

import org.omg.CORBA.PUBLIC_MEMBER;

/**
 * Created by dam on 17/02/17.
 */
public class Constantes {

    public static final String RUTA = "//localhost";
    public static final int PUERTO = 3306;
    public static final String USER = "root";
    public static final String PASSWD = "mysql";
    public static final String BBDD = "practica_datos_ra2";

    public static final String SELECT = "SELECT ";
    public static final String FROM = "FROM ";
    public static final String WHERE = "WHERE ";
    public static final String LIKE = "LIKE ";
    public static final String INSERT = "INSERT into ";
    public static final String UPDATE = "UPDATE ";
    public static final String VALUES = "VALUES ";
    public static final String DELETE = "DELETE from ";
    public static final String OR = "OR ";

    public static final String TB_GRANJERO = "granjero ";
    public static final String TB_CAMPO = "campo ";
    public static final String TB_MAQUINARIA = "maquinaria ";

    public static final String PROPS_USER = "user";
    public static final String PROPS_PASSWD = "passwd";
    public static final String PROPS_ADMINUSER = "adminUser";
    public static final String PROPS_ADMINPASSWD = "adminPasswd";
    public static final String PROPS_RUTADATOS = "rutaFicheroDatos";
    public static final String PROPS_RUTABBDD = "rutaBBDD";
    public static final String PROPS_PUERTO = "puerto";
    public static final String PROPS_SGBD = "sgbd";
    public static final String PROPS_BBDD = "nombreBBDD";



}
