package com.simion.util;

import com.simion.base.TipoMaquinaria;
import org.omg.CORBA.PUBLIC_MEMBER;

import javax.swing.*;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Simion on 09/11/2016.
 */
public class Util {

    public static int warningEliminar(String titulo, String mensaje){
        return JOptionPane.showConfirmDialog(null, mensaje, titulo, JOptionPane.YES_NO_OPTION);
    }

    public static double parseSuperficie(String superficie) throws ParseException {
        DecimalFormat df = new DecimalFormat("#,##0.00 m3");
        return df.parse(superficie).doubleValue();
    }

    public static void warningCampoVacio(String titulo, String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, titulo, JOptionPane.ERROR_MESSAGE);
    }

    public static String formatSuperficie(double superficie) {
        DecimalFormat df = new DecimalFormat("#,##0.00 m3");
        return df.format(superficie);
    }

    public static Date parseFecha(String fechaEnString) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
        return sdf.parse(fechaEnString);
    }

    public static java.sql.Date dateSQL(java.util.Date fechaDate){
        java.sql.Date fechaSQL = new java.sql.Date(fechaDate.getTime());
        return fechaSQL;
    }

    public static TipoMaquinaria parseTipoMaquina(String tipoMaquina){
        for (TipoMaquinaria tipoMaquinaria:TipoMaquinaria.values()){
            if(tipoMaquina.equals(tipoMaquinaria.toString())){
                return tipoMaquinaria;
            }
        }
        return null;
    }
}
