package com.simion.gui;

import com.simion.base.Campo;
import com.simion.base.Granjero;
import com.simion.base.Maquinaria;
import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

/**
 * Created by Simion on 06/11/2016.
 */
public class Ventana extends JFrame{
    JPanel pnlPrincipal;
    JButton btNuevo;
    JTabbedPane pnlTabPestaniero;
    JTextField tfDNI;
    JTextField tfNombre;
    JTextField tfApellidos;
    JList lsCamposGranjero;
    JComboBox cbCampos;
    JComboBox cbMaquinarias;
    JButton btAddCampo;
    JButton btAddMaquinaria;
    JList lsObjetos;
    JPanel pnlGranjero;
    JPanel pnlCampo;
    JComboBox cbTipoMaquinaria;
    JPanel pnlMaquinaria;
    JLabel lbDNI;
    JDateChooser dcFechaNac;
    JLabel Nombre;
    JLabel lbApellidos;
    JLabel lbFechaNac;
    JLabel lbCampo;
    JLabel lbMaquinaria;
    JLabel lbPosesiones;
    JButton btModificar;
    JButton btGuardar;
    JButton btCancelar;
    JButton btEliminar;
    JLabel lbReferencia;
    JLabel lbCampoNombre;
    JLabel lbSuperficie;
    JLabel lbFechaCompra;
    JLabel lbCampoPropietario;
    JTextField tfReferencia;
    JTextField tfCampoNombre;
    JTextField tfCampoSuperficie;
    JDateChooser dcCampoFechaCompra;
    JLabel lbCampoProp;
    JLabel lbMatricula;
    JLabel lbMarca;
    JLabel lbTipoMaquinaria;
    JLabel lbMaquinaFechaCompra;
    JLabel lbMaquinaPropietario;
    JTextField tfMatricula;
    JTextField tfMarca;
    JDateChooser dcMaquinariaFechaCompra;
    JLabel lbMaquinariaProp;
    JLabel lbBarraEstado;
    JLabel lbAutoGuardado;
    JSplitPane jspGrande;
    JSplitPane jspIzquierda;
    JPanel pnlBotonera;
    JList lsMaquinasGranjero;
    JLabel lbCamposGranjero;
    JLabel lbMaquinasGranjero;
    JButton btQuitCampo;
    JButton btQuitMaquina;
    JPanel pnlConfig;
    JTextField tfPuerto;
    JTextField tfRuta;
    JTextField tfBuscar;
    JButton btBuscar;
    JRadioButton rbMySQL;
    JRadioButton rbPostgreSQL;
    JButton btConfigAceptar;
    JButton btConfigDefault;
    JTextField tfBBDD;
    JTable tbTabla;
    JTextField tfUsuarioBBDD;
    DefaultListModel<Granjero> dlmGranjeros;
    DefaultListModel<Campo> dlmCampos;
    DefaultListModel<Maquinaria> dlmMaquinarias;
    DefaultListModel<Campo> dlmCamposGranjero;
    DefaultListModel<Maquinaria> dlmMaquinasGranjero;
    DefaultTableModel dlmTablaGranjero;
    DefaultTableModel dlmTablaCampo;
    DefaultTableModel dlmTablaMaquina;
    JMenuBar menuBar;
    JMenu menuArchivo;
    JMenu menuEditar;
    JMenu menuAyuda;
    JMenuItem itemAbrir;
    JMenuItem itemGuardar;
    JMenuItem itemGuardarComo;
    JMenuItem itemSalir;
    JMenuItem itemPreferencias;
    JMenuItem itemAcercaDe;
    JMenuItem itemAutoGuardar;
    JFileChooser jfcSelectorDirectorio;
    JFrame frame;

    public Ventana() {
        frame = new JFrame("Practica R.A. 4");
        frame.setJMenuBar(barraDeMenu());
        frame.setContentPane(pnlPrincipal);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        dlmGranjeros = new DefaultListModel<>();
        dlmCampos = new DefaultListModel<>();
        dlmMaquinarias = new DefaultListModel<>();
        dlmCamposGranjero = new DefaultListModel<>();
        dlmMaquinasGranjero = new DefaultListModel<>();

        iniciarTabla();

        lsObjetos.setModel(dlmGranjeros);
        lsCamposGranjero.setModel(dlmCamposGranjero);
        lsMaquinasGranjero.setModel(dlmMaquinasGranjero);
        jfcSelectorDirectorio = new JFileChooser();
        jfcSelectorDirectorio.setCurrentDirectory(new File(System.getProperty("user.dir") + File.separator+"datos"));

    }

    private void iniciarTabla() {
        dlmTablaGranjero = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int colum) {
                return false;
            }
        };
        dlmTablaCampo = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int colum) {
                return false;
            }
        };
        dlmTablaMaquina = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int colum) {
                return false;
            }
        };
        dlmTablaGranjero.addColumn("Id");
        dlmTablaGranjero.addColumn("DNI");
        dlmTablaGranjero.addColumn("Nombre");
        dlmTablaGranjero.addColumn("Apellidos");
        dlmTablaGranjero.addColumn("Fecha nacimiento");

        dlmTablaCampo.addColumn("Id");
        dlmTablaCampo.addColumn("Referencia");
        dlmTablaCampo.addColumn("Nombre");
        dlmTablaCampo.addColumn("Superficie en m3");
        dlmTablaCampo.addColumn("Fecha compra");
        dlmTablaCampo.addColumn("Id propietario");

        dlmTablaMaquina.addColumn("Id");
        dlmTablaMaquina.addColumn("Matricula");
        dlmTablaMaquina.addColumn("Marca");
        dlmTablaMaquina.addColumn("Tipo maquinaria");
        dlmTablaMaquina.addColumn("Fecha compra");
        dlmTablaMaquina.addColumn("Id propietario");
    }

    @Override
    public void dispose() {
        System.out.println("La aplicacion se ha cerrado.");
        super.dispose();
    }

    /**
     * Metodo que se encarga de gestionar la barra de menus del entrono gráfico
     * @return un objeto de tipo JMenuBar
     */
    public JMenuBar barraDeMenu() {
        menuBar = new JMenuBar();
        menuArchivo = new JMenu("Archivo");
        menuEditar = new JMenu("Editar");
        menuAyuda = new JMenu("Ayuda");
        itemAbrir = new JMenuItem("Abrir");
        itemGuardar = new JMenuItem("Guardar");
        itemGuardar.setActionCommand("GuardarAuto");
        itemGuardarComo = new JMenuItem("Guardar como");
        itemSalir = new JMenuItem("Salir");
        itemPreferencias = new JMenuItem("Cambiar ubicacion");
        itemAutoGuardar = new JMenuItem("AutoGuardar ON");
        itemAcercaDe = new JMenuItem("Acerca de");
        menuBar.add(menuArchivo);
        menuArchivo.add(itemAbrir);
        menuArchivo.add(itemGuardar);
        menuArchivo.add(itemGuardarComo);
        menuArchivo.add(itemSalir);
        menuBar.add(menuEditar);
        menuEditar.add(itemPreferencias);
        menuEditar.add(itemAutoGuardar);
        menuBar.add(menuAyuda);
        menuAyuda.add(itemAcercaDe);
        return menuBar;
    }

    /**
     * Metodo que obtiene la ruta de un fichero que se quiere abrir.
     * @return un String con el valor del ruta del path completo
     */
    public String obtenerAbrir() {
        jfcSelectorDirectorio.showOpenDialog(itemAbrir);
        return jfcSelectorDirectorio.getSelectedFile().toString();
    }

    /**
     * Metodo que obtiene la ruta de un fichero donde se quiere guardar.
     * @return un String con el valor del ruta del path completo
     */
    public String obtenerGuardarComo() {
        jfcSelectorDirectorio.showSaveDialog(itemGuardarComo);
        return jfcSelectorDirectorio.getSelectedFile().toString();
    }


}
