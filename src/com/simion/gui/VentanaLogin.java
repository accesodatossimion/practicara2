package com.simion.gui;

import sun.security.x509.X509CertInfo;

import javax.swing.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import static com.simion.util.Constantes.*;

/**
 * Clase que genera la ventana del login del programa
 * @author Simion Bordean
 */
public class VentanaLogin extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private String rutaFicheroInicio;
    boolean admin;
    JTextField tfUsuario;
    JPasswordField ptfContrasena;
    JLabel lbUsuario;
    JLabel lbContrasena;
    private String gestor;

    /**
     * Constructor del la ventana
     * @param rutaFicheroInicio ruta del fichedo para comprobar los datos de autenticacion.
     */
    public VentanaLogin(String rutaFicheroInicio) {
        this.rutaFicheroInicio = rutaFicheroInicio;
        setTitle("Login");
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * Metodo que gestionla la accion de pulsar el boton ok
     */
    private void onOK() {
        String usuario = "";
        String password = "";
        String adminUser = "";
        String adminPwd = "";
        try {
            Properties configuracion = new Properties();
            configuracion.load(new FileInputStream(rutaFicheroInicio));
            usuario = configuracion.getProperty(PROPS_USER);
            password = configuracion.getProperty(PROPS_PASSWD);
            adminUser = configuracion.getProperty(PROPS_ADMINUSER);
            adminPwd = configuracion.getProperty(PROPS_ADMINPASSWD);
            gestor = configuracion.getProperty(PROPS_SGBD);
        } catch (FileNotFoundException fnfe ) {
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        String charPassword = new String(ptfContrasena.getPassword());
        if (tfUsuario.getText().equals(usuario) && charPassword.equals(password)){
            admin = false;
            dispose();
        }
        else if (tfUsuario.getText().equals(adminUser) && charPassword.equals(adminPwd)){
            admin = true;
            dispose();
        }
        else {
            JOptionPane.showMessageDialog(null, "Error al ingresar la contraseña. Vuelva a intentarlo", "Error en login", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Metodo que gestikon la opcion de cancelar
     */
    private void onCancel() {
        // add your code here if necessary
        System.exit(0);
    }

    public String getGestor() {
        return gestor;
    }
}
