package com.simion.gui;

import com.simion.base.*;
import com.simion.util.Util;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static com.simion.util.Constantes.*;

/**
 * Clase que gestiona la informacion entre el Model y la Ventana
 * Created by Simion on 30/10/2016.
 * @author Simion Bordean
 * @version 1.0 final
 */
public class Controlador implements ActionListener, ChangeListener, ListSelectionListener, KeyListener{
    private Modelo modelo;
    private Ventana view;
    private enum Pestania{
        GRANJERO, CAMPO, MAQUINA, CONFIG;
    }
    private enum Accion {
        NUEVO, MODIFICAR
    }
    private Pestania pestania;
    private Accion accion;
    private double superficie;
    private boolean autoGuardar;

    /**
     * Constructor de la clase
     * @param modelo es el modelo necesacio con los datos de la aplicacion
     * @param view es la ventana que gestiona el entorno gráfico
     */
    public Controlador(Modelo modelo, Ventana view)
    {
        VentanaLogin v = new VentanaLogin(modelo.getRutaFicheroInicio());
        this.modelo = modelo;
        this.view = view;
        if(!v.admin) {
            view.pnlTabPestaniero.remove(3);
        }
        addListener();
        view.dcCampoFechaCompra.setEnabled(false);
        view.dcFechaNac.setEnabled(false);
        view.dcMaquinariaFechaCompra.setEnabled(false);
        view.tfCampoSuperficie.setSelectionStart(0);
        pestania = Pestania.GRANJERO;
        autoGuardar = true;
        for(TipoMaquinaria unTipo:TipoMaquinaria.values()){
            view.cbTipoMaquinaria.addItem(unTipo);
        }
        view.cbTipoMaquinaria.setSelectedIndex(0);
        if (v.getGestor().equals("mysql")){
            modelo.conectarMySQL();
        }
        else if (v.getGestor().equals("postgresql")){
            modelo.conectarPostgreSQL();
        }
        refrescarLista();
    }

    /**
     * Metodo que añade los listener necesarios de los diferentes componentes visuales.
     */
    private void addListener() {
        view.frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                modelo.desconectar();
                view.dispose();
                System.exit(0);
            }
        });

        view.btNuevo.addActionListener(this);
        view.btModificar.addActionListener(this);
        view.btGuardar.addActionListener(this);
        view.btCancelar.addActionListener(this);
        view.btEliminar.addActionListener(this);
        view.btAddCampo.addActionListener(this);
        view.btAddMaquinaria.addActionListener(this);
        view.btQuitCampo.addActionListener(this);
        view.btQuitMaquina.addActionListener(this);
        view.btBuscar.addActionListener(this);
        view.tfBuscar.addKeyListener(this);
        view.btConfigAceptar.addActionListener(this);
        view.btConfigDefault.addActionListener(this);

        view.pnlTabPestaniero.addChangeListener(this);
        view.lsObjetos.addListSelectionListener(this);
        view.lsCamposGranjero.addListSelectionListener(this);
        view.lsCamposGranjero.addKeyListener(this);
        view.lsMaquinasGranjero.addListSelectionListener(this);
        view.lsMaquinasGranjero.addKeyListener(this);

        view.itemAbrir.addActionListener(this);
        view.itemGuardar.addActionListener(this);
        view.itemGuardarComo.addActionListener(this);
        view.itemSalir.addActionListener(this);
        view.itemPreferencias.addActionListener(this);
        view.itemAutoGuardar.addActionListener(this);
        view.itemAcercaDe.addActionListener(this);
    }

    /**
     * Metodo que gestiona los eventos generados por el cambios de pestaña
     * del panel JTabbetPanel
     * @param e el es evento de quien lo ha generado
     */
    @Override
    public void stateChanged(ChangeEvent e) {
        if(view.pnlTabPestaniero.getSelectedIndex() == 0){
            pestania = Pestania.GRANJERO;
            refrescarLista();
        }
        else if(view.pnlTabPestaniero.getSelectedIndex() == 1){
            pestania = Pestania.CAMPO;
            refrescarLista();
        }
        else if (view.pnlTabPestaniero.getSelectedIndex() == 2){
            pestania = Pestania.MAQUINA;
            refrescarLista();
        }
        else if(view.pnlTabPestaniero.getSelectedIndex() == 3){
            pestania = Pestania.CONFIG;
        }
    }

    /**
     * Metodo que gestiona los eventos generados por las diferentes listas
     * del entorno gráfico
     * @param e es el evento de quien lo ha generado
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getSource().equals(view.lsObjetos)){
            view.lsCamposGranjero.clearSelection();
            view.lsMaquinasGranjero.clearSelection();
            limpiarGUI();
            switch (pestania){
                case GRANJERO:
                    Granjero unGranjero = (Granjero) view.lsObjetos.getSelectedValue();
                    if (unGranjero == null){
                        return;
                    }
                    view.tfDNI.setText(unGranjero.getDni());
                    view.tfNombre.setText(unGranjero.getNombre());
                    view.tfApellidos.setText(unGranjero.getApellidos());
                    view.dcFechaNac.setDate(unGranjero.getFechaNac());
                    rellenarCombos();
                    refrescarListaCampos(unGranjero);
                    refrescarListaMaquinas(unGranjero);
                    modoEdicionGranjero(false);
                    break;
                case CAMPO:
                    Campo unCampo = (Campo) view.lsObjetos.getSelectedValue();
                    if(unCampo == null){
                        return;
                    }
                    view.tfReferencia.setText(unCampo.getReferencia());
                    view.tfCampoNombre.setText(unCampo.getNombre());
                    view.tfCampoSuperficie.setText(Util.formatSuperficie(unCampo.getSuperficie()));
                    view.dcCampoFechaCompra.setDate(unCampo.getFechaCompra());
                    if (unCampo.getPropietario() == 0){
                        view.lbCampoProp.setText("");
                    }
                    else
                        view.lbCampoProp.setText(modelo.obtenerGranjero(unCampo.getPropietario()).toString());
                    modoEdicionCampo(false);
                    break;
                case MAQUINA:
                    Maquinaria unaMaquina = (Maquinaria) view.lsObjetos.getSelectedValue();
                    if(unaMaquina == null){
                        return;
                    }
                    view.tfMatricula.setText(unaMaquina.getMatricula());
                    view.tfMarca.setText(unaMaquina.getMarca());
                    view.cbTipoMaquinaria.setSelectedItem(unaMaquina.getTipoMaquinaria());
                    view.dcMaquinariaFechaCompra.setDate(unaMaquina.getFechaCompra());
                    if (unaMaquina.getPropietario() == 0){
                        view.lbMaquinariaProp.setText("");
                    }
                    else
                        view.lbMaquinariaProp.setText(modelo.obtenerGranjero(unaMaquina.getPropietario()).toString());
                    modoEdicionMaquina(false);
                    break;
                default:
                    break;
            }

        }
        else if(e.getSource().equals(view.lsCamposGranjero)){
            if(!view.lsCamposGranjero.isSelectionEmpty() && view.dlmCamposGranjero.getSize() != 0){
                view.btQuitCampo.setEnabled(true);
            }
            else
                view.btQuitCampo.setEnabled(false);
        }
        else if(e.getSource().equals(view.lsMaquinasGranjero)){
            if(!view.lsMaquinasGranjero.isSelectionEmpty() && view.dlmMaquinasGranjero.getSize() != 0){
                view.btQuitMaquina.setEnabled(true);
            }
            else
                view.btQuitMaquina.setEnabled(false);
        }
    }


    /**
     * Metodo que gestiona los eventos generados por los diferentes componentes
     * del entrono gráfico.
     * @param e es el evento de quien lo ha generado.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        String ruta;
        switch (comando){
            case "Abrir":
                if(!autoGuardar){
                    int salir = Util.warningEliminar("Guardar", "El Autoguardado esta DESACTIVADO. ¿Desea guardar antes de salir?");
                    if(salir == JOptionPane.YES_OPTION){
                        modelo.guardarFichero(modelo.getRutaFicheroDatos());
                    }
                }
                try{
                    ruta = view.obtenerAbrir();
                    modelo.cargarFichero(ruta);
                    modelo.setRutaFicheroDatos(ruta);
                    modelo.guardarRutaActual();
                    refrescarLista();
                    view.lbBarraEstado.setText("Archivo abierto con exito de la ruta: "+ruta);
                }catch (NullPointerException npe){
                    Util.warningCampoVacio("Error de Fichero", "NO ha indicado un fichero a abrir.");
                }
                break;
            case "GuardarAuto":
                modelo.guardarFichero(modelo.getRutaFicheroDatos());
                break;
            case "Guardar como":
                try {
                    ruta = view.obtenerGuardarComo();
                    modelo.guardarFichero(ruta);
                    view.lbBarraEstado.setText("Datos guardados en: "+ruta);
                }catch (NullPointerException npe){
                    Util.warningCampoVacio("Error de Fichero", "NO ha indicado un fichero a guardar.");
                }
                break;
            case "Salir":
                System.exit(0);
                break;
            case "Cambiar ubicacion":
                try {
                    ruta = view.obtenerGuardarComo();
                    modelo.setRutaFicheroDatos(ruta);
                    view.lbBarraEstado.setText("Ubicacion cambiada a: "+ruta);
                }catch (NullPointerException npe){
                    Util.warningCampoVacio("Error de Fichero", "NO ha indicado un fichero a guardar.");
                }
                break;
            case "AutoGuardar ON":
                autoGuardar = true;
                view.itemAutoGuardar.setText("AutoGuardar OFF");
                view.lbAutoGuardado.setText("Auto Guardado esta DESACTIVADO");
                break;
            case "AutoGuardar OFF":
                autoGuardar = false;
                view.itemAutoGuardar.setText("AutoGuardar ON");
                view.lbAutoGuardado.setText("Auto Guardado esta ACTIVADO");
                break;
            case "Acerca de":
                JOptionPane.showMessageDialog(null, "Programa diseñado por Simion Bordean sobre \n" +
                        "la gestion de una Granja.", "Acerca del Programa", JOptionPane.INFORMATION_MESSAGE);
                break;
            case "buscar":
                buscar(view.tfBuscar.getText());
                break;
            case "ConfigAceptar":
                int puerto;
                try{
                    puerto = Integer.parseInt(view.tfPuerto.getText());
                } catch (NumberFormatException nfe){
                    Util.warningCampoVacio("Error de puerto", "El puerto indicado es incorrecto");
                    return;
                }
                int cambiar = Util.warningEliminar("Cambiar configuracion", "¿Esta seguro de que quiere cambiar la configuracion de conexion ?");
                if (cambiar == JOptionPane.YES_OPTION){
                    String sgbd = "";
                    if (view.rbMySQL.isSelected()){
                        sgbd = "mysql";
                    }
                    else if (view.rbPostgreSQL.isSelected()){
                        sgbd = "postgresql";
                    }
                    String servidor = view.tfRuta.getText();
                    String bbdd = view.tfBBDD.getText();
                    String userbbdd = view.tfUsuarioBBDD.getText();
                    modelo.cambiarConfigConexion(sgbd, servidor, puerto, bbdd);
                }
                else
                    return;
                break;
            case "ConfigDefault":
                int cargarDefault = Util.warningEliminar("Cargar config por defecto", "¿Esta seguro de que quiere cargar la configuracion por defecto de conexion ?");
                if (cargarDefault == JOptionPane.YES_OPTION){
                    view.rbMySQL.setSelected(true);
                    view.tfRuta.setText(RUTA);
                    view.tfPuerto.setText(String.valueOf(PUERTO));
                    view.tfBBDD.setText(BBDD);
                    String sgbd = "mysql";
                    String servidor = view.tfRuta.getText();
                    puerto = Integer.parseInt(view.tfPuerto.getText());
                    String bbdd = view.tfBBDD.getText();
                    modelo.cambiarConfigConexion(sgbd, servidor, puerto, bbdd);
                }
                else
                    return;
                break;
            default:
                switch (pestania){
                    case GRANJERO:
                        gestionarGranjero(comando);
                        break;
                    case CAMPO:
                        gestionarCampo(comando);
                        break;
                    case MAQUINA:
                        gestionarMaquina(comando);
                        break;
                    default:
                        break;
                }
                break;
        }
    }
    private void buscar(String texto) {
        texto = "%"+texto+"%";
        if (pestania.equals(Pestania.GRANJERO)){
            view.dlmGranjeros.clear();
            for (Granjero granjero: modelo.buscarGranjero(texto)){
                view.dlmGranjeros.addElement(granjero);
            }
        }
        else if (pestania.equals(Pestania.CAMPO)){
            view.dlmCampos.clear();
            for (Campo campo : modelo.buscarCampos(texto)){
                view.dlmCampos.addElement(campo);
            }
        }
        else if (pestania.equals(Pestania.MAQUINA)){
            view.dlmMaquinarias.clear();
            for (Maquinaria maquina : modelo.buscarMaquinas(texto)){
                view.dlmMaquinarias.addElement(maquina);
            }
        }
        // TODO COMPLETAR DE REALIZAR LAS BUSQUEDAS DE LOS 2 OBJETOS QUE FALTAN.
    }

    /**
     * Metodo que gestiona la tecla pulsada e impresa
     * @param e el evento de la tecla que lo ha generado
     */
    @Override
    public void keyTyped(KeyEvent e) {
    }

    /**
     * Metodo que gestiona los eventos por las teclas apretadas
     * @param e el evendo de la tecla que lo ha generado
     */
    @Override
    public void keyPressed(KeyEvent e) {
    }

    /**
     * Metodo que gestiona los eventos de las teclas soltadas
     * @param e el evento de la tecla que lo ha generado
     */
    @Override
    public void keyReleased(KeyEvent e) {
        //fixme
        if (e.getSource().equals(view.tfBuscar)){
            buscar(view.tfBuscar.getText());
        }
        if(e.getKeyCode() == KeyEvent.VK_DELETE && view.btGuardar.isEnabled()){
            if(!view.lsCamposGranjero.isSelectionEmpty()){
                Granjero elGranj = (Granjero) view.lsObjetos.getSelectedValue();
                view.cbCampos.addItem(view.lsCamposGranjero.getSelectedValue());
                view.dlmCamposGranjero.remove(view.lsCamposGranjero.getSelectedIndex());
                refrescarListaCampos(elGranj);

            }
            if(!view.lsMaquinasGranjero.isSelectionEmpty()){
                Granjero unGranjero = (Granjero) view.lsObjetos.getSelectedValue();
                view.cbMaquinarias.addItem(view.lsMaquinasGranjero.getSelectedValue());
                view.dlmMaquinasGranjero.remove(view.lsMaquinasGranjero.getSelectedIndex());
                refrescarListaMaquinas(unGranjero);
            }
        }
    }

    /**
     * Metodo que gestiona la informacion añadida a los campos
     * del entrono gráfico de la seccion de granjero.
     * @param comando es el evento del componente que lo ha generado
     */
    private void gestionarGranjero(String comando) {
        switch (comando){
            case "Nuevo":
                limpiarGUI();
                rellenarCombos();
                modoEdicionGranjero(true);
                accion = Accion.NUEVO;
                view.lbBarraEstado.setText("Nuevo granjero");
                break;
            case "Modificar":
                modoEdicionGranjero(true);
                accion = Accion.MODIFICAR;
                view.lbBarraEstado.setText("Modificando el granjero con DNI: " +
                        view.lsObjetos.getSelectedValue().toString());
                break;
            case "Guardar":
                while(!comprobarCamposObligatorios()){
                    return;
                }
                List<Integer> lista = new ArrayList<>();
                List<Integer> lista2 = new ArrayList<>();
                Granjero unGranjero;
                if(accion.equals(Accion.MODIFICAR)){
                    unGranjero = (Granjero) view.lsObjetos.getSelectedValue();
                }
                else
                    unGranjero = new Granjero();
                unGranjero.setDni(view.tfDNI.getText());
                unGranjero.setNombre(view.tfNombre.getText());
                unGranjero.setApellidos(view.tfApellidos.getText());
                unGranjero.setFechaNac(view.dcFechaNac.getDate());
                System.out.println(view.dlmMaquinasGranjero.getSize());
                for(int i = 0; i<view.dlmCamposGranjero.getSize(); i++){
                    lista.add(view.dlmCamposGranjero.get(i).getId());
                }
                unGranjero.setListaCampos(lista);
                for(int i = 0; i<view.dlmMaquinasGranjero.getSize(); i++){
                    lista2.add(view.dlmMaquinasGranjero.get(i).getId());
                }
                unGranjero.setListaMaquinas(lista2);
                if (accion.equals(Accion.MODIFICAR)){
                    modelo.modificarGranjero(unGranjero);
                }
                else
                    modelo.registrarGranjero(unGranjero);
                refrescarLista();
                view.lbBarraEstado.setText("Granjero guardado");
                break;
            case "Cancelar":
                refrescarLista();
                modoEdicionGranjero(false);
                view.lbBarraEstado.setText("Accion cancelada.");
                break;
            case "Eliminar":
                int eliminar = Util.warningEliminar("Eliminar granjero", "¿Esta seguro de que desea eliminar " +
                        "el granjero con DNI: "+view.lsObjetos.getSelectedValue().toString()+"?");
                if(eliminar == JOptionPane.YES_OPTION){
                    modelo.eliminarGranjero((Granjero) view.lsObjetos.getSelectedValue());
                }
                else{
                    view.lbBarraEstado.setText("Accion cancelada.");
                    return;
                }
                limpiarGUI();
                refrescarLista();
                view.lbBarraEstado.setText("Granjero eliminado.");
                break;
            case "addCampo":
                view.dlmCamposGranjero.addElement((Campo) view.cbCampos.getSelectedItem());
                view.cbCampos.removeItem(view.cbCampos.getSelectedItem());
                view.lbBarraEstado.setText("Campo agregado");
                break;
            case "elimCampo":
                Campo campo = view.dlmCamposGranjero.get(view.lsCamposGranjero.getSelectedIndex());
                modelo.borrarPropietario(TB_CAMPO, campo.getId());
                view.cbCampos.addItem(campo);
                view.dlmCamposGranjero.removeElementAt(view.lsCamposGranjero.getSelectedIndex());
                view.lbBarraEstado.setText("Campo eliminado");
                break;
            case "addMaquina":
                view.dlmMaquinasGranjero.addElement((Maquinaria) view.cbMaquinarias.getSelectedItem());
                view.cbMaquinarias.removeItem(view.cbMaquinarias.getSelectedItem());
                view.lbBarraEstado.setText("Maquina agregada.");
                break;
            case "elimMaquina":
                Maquinaria maquina = view.dlmMaquinasGranjero.get(view.lsMaquinasGranjero.getSelectedIndex());
                modelo.borrarPropietario(TB_MAQUINARIA, maquina.getId());
                view.cbMaquinarias.addItem(maquina);
                view.dlmMaquinasGranjero.removeElementAt(view.lsMaquinasGranjero.getSelectedIndex());
                view.lbBarraEstado.setText("Maquina eliminada");
                break;
            default:
                break;
        }
    }

    /**
     * Metodo que gestiona la informacion añadida a los campos
     * del entrono gráfico de la seccion de Campo.
     * @param comando es el evento del componente que lo ha generado
     */
    private void gestionarCampo(String comando) {
        switch (comando) {
            case "Nuevo":
                limpiarGUI();
                modoEdicionCampo(true);
                accion = Accion.NUEVO;
                view.lbBarraEstado.setText("Nuevo campo.");
                break;
            case "Modificar":
                modoEdicionCampo(true);
                accion = Accion.MODIFICAR;
                view.lbBarraEstado.setText("Modificando el campo con referencia: " +
                        view.lsObjetos.getSelectedValue().toString());
                break;
            case "Guardar":
                while(!comprobarCamposObligatorios()){
                    return;
                }
                Campo unCampo;
                if(accion.equals(Accion.MODIFICAR)){
                    unCampo = (Campo) view.lsObjetos.getSelectedValue();
                }
                else
                    unCampo = new Campo();
                unCampo.setReferencia(view.tfReferencia.getText());
                unCampo.setNombre(view.tfCampoNombre.getText());
                unCampo.setSuperficie(superficie);
                unCampo.setFechaCompra(view.dcCampoFechaCompra.getDate());
                if (accion.equals(Accion.MODIFICAR)){
                    modelo.modificarCampo(unCampo);
                }
                else{
                    modelo.registrarCampo(unCampo);
                }
                refrescarLista();
                view.lbBarraEstado.setText("Campo guardado.");
                break;
            case "Cancelar":
                modoEdicionCampo(false);
                view.lbBarraEstado.setText("Accion cancelada.");
                break;
            case "Eliminar":
                int eliminar = Util.warningEliminar("Eliminar campo", "¿Esta seguro de que desea eliminar " +
                        "el campo con referencia: "+view.lsObjetos.getSelectedValue().toString()+"?");
                if(eliminar == JOptionPane.YES_OPTION){
                    modelo.eliminarCampo((Campo) view.lsObjetos.getSelectedValue());
                }
                else{
                    view.lbBarraEstado.setText("Accion cancelada.");
                    return;
                }
                limpiarGUI();
                refrescarLista();
                view.lbBarraEstado.setText("Campo eliminado.");
                break;
            default:
                break;
        }
    }

    /**
     * Metodo que gestiona la informacion añadida a los campos
     * del entrono gráfico de la seccion de Maquinaria.
     * @param comando es el evento del componente que lo ha generado
     */
    private void gestionarMaquina(String comando) {
        switch (comando) {
            case "Nuevo":
                limpiarGUI();
                modoEdicionMaquina(true);
                accion = Accion.NUEVO;
                view.lbBarraEstado.setText("Nueva maquina.");
                break;
            case "Modificar":
                modoEdicionMaquina(true);
                accion = Accion.MODIFICAR;
                view.lbBarraEstado.setText("Modificando la maquina con matricula: " +
                        view.lsObjetos.getSelectedValue().toString());
                break;
            case "Guardar":
                while(!comprobarCamposObligatorios()){
                    return;
                }
                Maquinaria unaMaquina;
                if(accion.equals(Accion.MODIFICAR)){
                    unaMaquina = (Maquinaria) view.lsObjetos.getSelectedValue();
                }
                else
                    unaMaquina = new Maquinaria();
                unaMaquina.setMatricula(view.tfMatricula.getText());
                unaMaquina.setMarca(view.tfMarca.getText());
                unaMaquina.setTipoMaquinaria((TipoMaquinaria) view.cbTipoMaquinaria.getSelectedItem());
                unaMaquina.setFechaCompra(view.dcMaquinariaFechaCompra.getDate());
                if (accion.equals(Accion.MODIFICAR)){
                    modelo.modificarMaquina(unaMaquina);
                }
                else{
                    modelo.registrarMaquinaria(unaMaquina);
                }
                refrescarLista();
                view.lbBarraEstado.setText("Maquina guardada.");
                break;
            case "Cancelar":
                modoEdicionMaquina(false);
                view.lbBarraEstado.setText("Accion cancelada.");
                break;
            case "Eliminar":
                int eliminar = Util.warningEliminar("Eliminar maquina", "¿Esta seguro de que desea eliminar " +
                        "la maquina con matricula: "+view.lsObjetos.getSelectedValue().toString()+"?");
                if(eliminar == JOptionPane.YES_OPTION){
                    modelo.eliminarMaquina((Maquinaria) view.lsObjetos.getSelectedValue());
                }
                else{
                    view.lbBarraEstado.setText("Accion cancelada.");
                    return;
                }
                limpiarGUI();
                refrescarLista();
                view.lbBarraEstado.setText("Mquina eliminada.");
                break;
            default:
                break;
        }
    }

    /**
     * Metodo que se encarga de rellenar los comboBox del
     * la seccion de granjero.
     */
    private void rellenarCombos() {
        view.cbCampos.removeAllItems();
        view.cbMaquinarias.removeAllItems();
        for(Campo unCampo:modelo.obtenerCampos()){
            if(unCampo.getPropietario() == 0){
                view.cbCampos.addItem(unCampo);
            }
        }
        for(Maquinaria unaMaquina:modelo.obtenerMaquinarias()){
            if(unaMaquina.getPropietario() == 0){
                view.cbMaquinarias.addItem(unaMaquina);
            }
        }
    }

    /**
     * Metodo que se encarga de resetear los campos del entrono gráfico.
     */
    private void limpiarGUI() {
        switch (pestania){
            case GRANJERO:
                view.tfDNI.setText("");
                view.tfNombre.setText("");
                view.tfApellidos.setText("");
                view.dcFechaNac.setDate(null);
                view.cbCampos.removeAllItems();
                view.cbMaquinarias.removeAllItems();
                view.dlmCamposGranjero.clear();
                view.dlmMaquinasGranjero.clear();
                break;
            case CAMPO:
                view.tfReferencia.setText("");
                view.tfCampoNombre.setText("");
                view.tfCampoSuperficie.setText("0 m3");
                view.dcCampoFechaCompra.setDate(null);
                view.lbCampoProp.setText("");
                break;
            case MAQUINA:
                view.tfMatricula.setText("");
                view.tfMarca.setText("");
                view.cbTipoMaquinaria.setSelectedItem(0);
                view.dcMaquinariaFechaCompra.setDate(null);
                view.lbMaquinariaProp.setText("");
                break;
            default:
                break;
        }
    }

    /**
     * Metodo que se encarga de actualizar la información de la lista
     * principal del entorno gráfico.
     */
    private void refrescarLista(){
        Object[] fila;
        switch (pestania){
            case GRANJERO:
                view.dlmGranjeros.clear();
                view.dlmCamposGranjero.clear();
                view.dlmMaquinasGranjero.clear();
                view.dlmTablaGranjero.setNumRows(0);
                view.lsObjetos.setModel(view.dlmGranjeros);
                for(Granjero unGranjero:modelo.obtenerGranjeros()){
                    view.dlmGranjeros.addElement(unGranjero);
                    fila = new Object[]{unGranjero.getId(), unGranjero.getDni(),
                            unGranjero.getNombre(), unGranjero.getApellidos(),
                            unGranjero.getFechaNac()
                    };
                    view.dlmTablaGranjero.addRow(fila);
                }
                view.tbTabla.setModel(view.dlmTablaGranjero);
                modoEdicionGranjero(false);
                break;
            case CAMPO:
                view.dlmCampos.clear();
                view.dlmTablaCampo.setNumRows(0);
                view.lsObjetos.setModel(view.dlmCampos);
                for(Campo unCampo:modelo.obtenerCampos()){
                    view.dlmCampos.addElement(unCampo);
                    fila = new Object[]{unCampo.getId(), unCampo.getReferencia(),
                            unCampo.getNombre(), unCampo.getSuperficie(),
                            unCampo.getFechaCompra(), unCampo.getPropietario()
                    };
                    view.dlmTablaCampo.addRow(fila);
                }
                view.tbTabla.setModel(view.dlmTablaCampo);
                modoEdicionCampo(false);
                break;
            case MAQUINA:;
                view.dlmMaquinarias.clear();
                view.dlmTablaMaquina.setNumRows(0);
                view.lsObjetos.setModel(view.dlmMaquinarias);
                for(Maquinaria unaMaquina:modelo.obtenerMaquinarias()){
                    view.dlmMaquinarias.addElement(unaMaquina);
                    fila = new Object[]{unaMaquina.getId(), unaMaquina.getMatricula(),
                            unaMaquina.getMarca(), unaMaquina.getTipoMaquinaria(),
                            unaMaquina.getFechaCompra(), unaMaquina.getPropietario()
                    };
                    view.dlmTablaMaquina.addRow(fila);
                }
                view.tbTabla.setModel(view.dlmTablaMaquina);
                modoEdicionMaquina(false);
                break;
            case CONFIG:
                Properties props = new Properties();
                try {
                    props.load(new FileInputStream(modelo.getRutaFicheroInicio()));
                    view.tfRuta.setText(props.getProperty(PROPS_RUTABBDD));
                    view.tfPuerto.setText(props.getProperty(PROPS_PUERTO));
                    view.tfBBDD.setText(props.getProperty(PROPS_BBDD));
                    if (props.getProperty(PROPS_SGBD).equals("mysql")){
                        view.rbMySQL.setSelected(true);
                    }
                    else if (props.getProperty(PROPS_SGBD).equals("postgresql")){
                        view.rbPostgreSQL.setSelected(true);
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                break;
            default:
                break;
        }
        if(autoGuardar){
            view.lbAutoGuardado.setText("Auto Guardado esta ACTIVADO");
        }
        else{
            view.lbAutoGuardado.setText("Auto Guardado esta DESCATIVADO");
        }
    }

    /**
     * Metodo que se encarga de actualizar la informacion de
     * la lista de campos de un granjero en el entorno grafico
     */
    private void refrescarListaCampos(Granjero elGranjero) {
        view.dlmCamposGranjero.clear();
        for (int idCampo:elGranjero.getListaCampos()){
            view.dlmCamposGranjero.addElement(modelo.obtenerCampo(idCampo));
        }
    }

    /**
     * Metodo que se encarga de actualizar la infromación de
     * la lista de maquinarias de un granjero en el entorno grafico
     */
    private void refrescarListaMaquinas(Granjero elGranjero) {
        view.dlmMaquinasGranjero.clear();
        for (int idMaquina:elGranjero.getListaMaquinas()){
            view.dlmMaquinasGranjero.addElement(modelo.obtenerMaquinaria(idMaquina));
        }
    }

    /**
     * Metodo que comprueba que los campos obligatorios no tengan
     * un formato error o que esten vacios.
     * @return un boolean para confirmar que los campos son correctos
     */
    private boolean comprobarCamposObligatorios() {
        switch (pestania){
            case GRANJERO:
                if(view.tfDNI.getText().equals("")){
                    String mensaje = "El campo DNI no puede estar vacio";
                    String titulo = "error";
                    Util.warningCampoVacio(titulo, mensaje);
                    return false;
                }
                break;
            case CAMPO:
                if(view.tfReferencia.getText().equals("")){
                    String mensaje = "El campo Referencia no puede estar vacio.";
                    String titulo = "Error";
                    Util.warningCampoVacio(titulo, mensaje);
                    return false;
                }
                try{
                    superficie = Util.parseSuperficie(view.tfCampoSuperficie.getText());
                } catch (ParseException e) {
                    String mensaje = "Error en el formato del campo superficie. 0.00 m3";
                    String titulo = "Error en superfice";
                    Util.warningCampoVacio(titulo, mensaje);
                    return false;
                }
                break;
            case MAQUINA:
                if(view.tfMatricula.getText().equals("")){
                    String mensaje = "El campo Matricula no puede estar vacio.";
                    String titulo = "Error";
                    Util.warningCampoVacio(titulo, mensaje);
                    return false;
                }
                break;
            default:
                break;
        }
        return true;
    }

    /**
     * Metodo que se encarga de modificar la edicion de los diferentes
     * campos del entrono gráfico de la secccion de Granjero
     * @param b el valor boolean del estado del modo de edicion
     */
    private void modoEdicionGranjero(boolean b) {
        modoEdicionBotones(b);
        view.tfDNI.setEnabled(b);
        view.tfNombre.setEnabled(b);
        view.tfApellidos.setEnabled(b);
        view.dcFechaNac.setEnabled(b);
        view.cbCampos.setEnabled(b);
        view.cbMaquinarias.setEnabled(b);
        if(view.cbCampos.getItemCount() != 0){
            view.btAddCampo.setEnabled(b);
        }
        else {
            view.btAddCampo.setEnabled(false);
        }
        if(view.cbMaquinarias.getItemCount() != 0 ){
            view.btAddMaquinaria.setEnabled(b);
        }
        else {
            view.btAddMaquinaria.setEnabled(false);
        }
        view.lsCamposGranjero.setEnabled(b);
        view.lsMaquinasGranjero.setEnabled(b);
    }

    /**
     * Metodo que se encarga de modificar la edicion de los diferentes
     * campos del entrono gráfico de la secccion de Campo
     * @param b el valor boolean del estado del modo de edicion
     */
    private void modoEdicionCampo(boolean b) {
        modoEdicionBotones(b);
        view.tfReferencia.setEnabled(b);
        view.tfCampoNombre.setEnabled(b);
        view.tfCampoSuperficie.setEnabled(b);
        view.dcCampoFechaCompra.setEnabled(b);
    }

    /**
     * Metodo que se encarga de modificar la edicion de los diferentes
     * campos del entrono gráfico de la secccion de Maquinaria
     * @param b el valor boolean del estado del modo de edicion
     */
    private void modoEdicionMaquina(boolean b) {
        modoEdicionBotones(b);
        view.tfMatricula.setEnabled(b);
        view.tfMarca.setEnabled(b);
        view.cbTipoMaquinaria.setEnabled(b);
        view.dcMaquinariaFechaCompra.setEnabled(b);
    }

    /**
     * Metodo que se encarga de gestionar la habilitacion y deshabilitacion
     * de los botones del entorno gráfico
     * @param b el valor boolean del estado del modo de edicion
     */
    private void modoEdicionBotones(boolean b) {
        view.pnlTabPestaniero.setEnabled(!b);
        view.lsObjetos.setEnabled(!b);
        view.btBuscar.setEnabled(!b);
        view.tfBuscar.setEnabled(!b);
        view.btNuevo.setEnabled(!b);
        if(view.lsObjetos.isSelectionEmpty()){
            view.btModificar.setEnabled(false);
        }
        else{
            view.btModificar.setEnabled(!b);
        }
        view.btGuardar.setEnabled(b);
        view.btCancelar.setEnabled(b);
        if(view.lsObjetos.isSelectionEmpty()){
            view.btEliminar.setEnabled(false);
        }
        else{
            view.btEliminar.setEnabled(!b);
        }
    }

}
