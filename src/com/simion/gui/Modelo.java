package com.simion.gui;

import com.simion.base.Campo;
import com.simion.base.Granjero;
import com.simion.base.Maquinaria;
import com.simion.util.Util;
import org.postgresql.util.PSQLException;

import java.io.*;
import java.sql.*;
import java.util.*;

import static com.simion.util.Constantes.*;

/**
 * Clase que se encarga de gestionar los datos del programa
 * Created by Simion on 30/10/2016.
 * @author Simion Bordean
 * @version 1.0 final
 */
public class Modelo{
    private ArrayList<Granjero> listaGranjeros;
    private ArrayList<Campo> listaCampos;
    private ArrayList<Maquinaria> listaMaquinarias;
    private String rutaFicheroDatos = System.getProperty("user.dir")+File.separator+"datos"+File.separator + "granja.dat";
    private String rutaFicheroInicio = "datos" + File.separator + "configuracion.props";

    private Connection db;

    /**
     * Constructor de la clase
     */
    public Modelo(){
        File fichero = new File (System.getProperty("user.dir") + File.separator+"datos");
        if(!fichero.isDirectory()){
            fichero.mkdirs();
        }
        fichero = new File(rutaFicheroInicio);
        if(!fichero.exists()){
            crearFicheroProps();
        }
        else {
            listaGranjeros = new ArrayList<>();
            listaCampos = new ArrayList<>();
            listaMaquinarias = new ArrayList<>();
        }

    }

    /**
     * Metodo que se encarga de gestionar la informacion del login del programa
     */
    private void crearFicheroProps() {
        String miUsuario = "user";
        String miContrasena = "user";
        String adminUsuario = "admin";
        String adminContrasena = "admin";
        String rutaDatos = rutaFicheroDatos;
        String rutaBBDD = "//localhost";
        String puerto = "3306";
        String sgbd = "mysql";
        String nombreBBDD = "practica_datos_ra2";

        Properties configuracion = new Properties();
        configuracion.setProperty(PROPS_USER, miUsuario);
        configuracion.setProperty(PROPS_PASSWD, miContrasena);
        configuracion.setProperty(PROPS_ADMINUSER, adminUsuario);
        configuracion.setProperty(PROPS_ADMINPASSWD, adminContrasena);
        configuracion.setProperty(PROPS_RUTADATOS, rutaDatos);
        configuracion.setProperty(PROPS_RUTABBDD, rutaBBDD);
        configuracion.setProperty(PROPS_PUERTO, puerto);
        configuracion.setProperty(PROPS_SGBD, sgbd);
        configuracion.setProperty(PROPS_BBDD, nombreBBDD);
        try {
            configuracion.store(new FileOutputStream(rutaFicheroInicio), "Fichero de configuracion");
        } catch (FileNotFoundException fnfe ) {
            Util.warningCampoVacio("Fichero no encontrado", "El archivo: " + rutaFicheroInicio + "" +
                    "NO se ha encontrado");
        } catch (IOException ioe) {
            Util.warningCampoVacio("Error de lectura", "El archivo: " + rutaFicheroInicio + "" +
                    "NO se puede leer.");
        }
    }

    /**
     * Metodo que se encarga de conectar con la base de datos mediante MySQL.
     */
    public void conectarMySQL() {
        Properties props = new Properties();
        try {
            props.load(new FileInputStream(rutaFicheroInicio));
            String rutaBBDD = props.getProperty(PROPS_RUTABBDD);
            int puerto = Integer.parseInt(props.getProperty(PROPS_PUERTO));
            String nombreBBDD = props.getProperty(PROPS_BBDD);
            Class.forName("com.mysql.jdbc.Driver");
            db = DriverManager.getConnection("jdbc:mysql:" + rutaBBDD + ":" + puerto + "/" + nombreBBDD,
                    USER, PASSWD);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Util.warningCampoVacio("Error de conexion", "Error al conectar con la base de datos mediante MySQL.\n" +
                    "El error es el siguiente: \n" + sw.toString());
            System.exit(0);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que se encarga de conectar con la base de datos mediante PostgreSQL.
     */
    public void conectarPostgreSQL() {
        Properties props = new Properties();
        try {
            props.load(new FileInputStream(rutaFicheroInicio));
            String rutaBBDD = props.getProperty(PROPS_RUTABBDD);
            int puerto = Integer.parseInt(props.getProperty(PROPS_PUERTO));
            String nombreBBDD = props.getProperty(PROPS_BBDD);
            String sgbd = props.getProperty(PROPS_SGBD);
            Class.forName("org.postgresql.Driver");
            db = DriverManager.getConnection("jdbc:"+ sgbd + ":"  + rutaBBDD + ":" +
                            puerto+ "/" + nombreBBDD, USER, PASSWD);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (PSQLException psqle) {
            StringWriter sw = new StringWriter();
            psqle.printStackTrace(new PrintWriter(sw));
            Util.warningCampoVacio("Error de conexion", "Error al conectar con la base de datos mediante PostgreSQL.\n" +
                    "El error es el siguiente: \n" + sw.toString());
            System.exit(0);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Metodo que se encarga de desconectar el programa de la base de dataos.
     */
    public void desconectar(){
        try {
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que se encarga de cargar los datos desde un fechero de ruta especificada por parametro
     * @param rutaFichero es la ruta del fichero a cargar
     */
    public void cargarFichero(String rutaFichero){
        try{
            ObjectInputStream desserializador = new ObjectInputStream(new FileInputStream(rutaFichero));
            listaGranjeros = (ArrayList<Granjero>) desserializador.readObject();
            listaCampos = (ArrayList<Campo>) desserializador.readObject();
            listaMaquinarias = (ArrayList<Maquinaria>) desserializador.readObject();
            desserializador.close();
        } catch (FileNotFoundException e) {
            Util.warningCampoVacio("Fichero no encontrado", "Error en la carga de archivo. " +
                    "El archivo: " + rutaFichero + "NO se ha encontrado");
        } catch (IOException ioe) {
            Util.warningCampoVacio("Error de lectura", "Error en la carga de archivo." +
                    "El archivo: " + rutaFichero + "NO se puede leer.");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que se encarga de guardar a un fichero la los datos del programa
     * @param rutaFichero el la ruta del fichero donde se quiere guardar.
     */
    public void guardarFichero(String rutaFichero) {
        try{
            ObjectOutputStream serializador = new ObjectOutputStream(new FileOutputStream(rutaFichero));
            serializador.writeObject(listaGranjeros);
            serializador.writeObject(listaCampos);
            serializador.writeObject(listaMaquinarias);
            if(serializador != null){
                serializador.close();
            }
        } catch (FileNotFoundException e) {
            Util.warningCampoVacio("Fichero no encontrado", "Error en la carga de archivo. " +
                    "El archivo: " + rutaFichero + "NO se ha encontrado");
        } catch (IOException ioe) {
            Util.warningCampoVacio("Error de lectura", "Error en la carga de archivo." +
                    "El archivo: " + rutaFichero + "NO se puede leer.");
        }
    }

    /**
     * Metodo que obtiene una lista de los granjeros que hay añadidos al programa
     * @return la lista de granjeros del programa
     */
    public Collection<Granjero> obtenerGranjeros() {
        String sentencia = SELECT + " * " + FROM + TB_GRANJERO;
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = db.prepareStatement(sentencia);
            rs = ps.executeQuery();
            ArrayList<Granjero> lista = new ArrayList<>();
            Granjero granjero = null;
            while (rs.next()){
                granjero = new Granjero();
                granjero.setId(rs.getInt(1));
                granjero.setDni(rs.getString(2));
                granjero.setNombre(rs.getString(3));
                granjero.setApellidos(rs.getString(4));
                granjero.setFechaNac(rs.getDate(5));
                ArrayList<Integer> lista2 = new ArrayList<>();
                for (Campo campo:obtenerCampos()){
                    if (campo.getPropietario() == granjero.getId()){
                        lista2.add(campo.getId());
                    }
                }
                granjero.setListaCampos(lista2);
                lista2 = new ArrayList<>();
                for (Maquinaria maquinaria: obtenerMaquinarias()){
                    if (maquinaria.getPropietario() == granjero.getId()){
                        lista2.add(maquinaria.getId());
                    }
                }
                granjero.setListaMaquinas(lista2);
                lista.add(granjero);
            }
            return lista;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void registrarGranjero(Granjero unGranjero) {
        String sentencia = INSERT + TB_GRANJERO + "(dni, nombre, apellidos, fecha_nac) " +
                VALUES + "(?, ?, ?, ?)";
        PreparedStatement ps = null;
        try {
            ps = db.prepareStatement(sentencia);
            ps.setString(1, unGranjero.getDni());
            ps.setString(2, unGranjero.getNombre());
            ps.setString(3, unGranjero.getApellidos());
            if (unGranjero.getFechaNac() == null)
                ps.setDate(4, null);
            else
                ps.setDate(4, Util.dateSQL(unGranjero.getFechaNac()));
            ps.executeUpdate();
            sentencia = SELECT + "* " + FROM + TB_GRANJERO + WHERE +
                    "dni = ? and nombre = ? and " +
                    "apellidos = ? and fecha_nac = ?";
            ps = db.prepareStatement(sentencia);
            ps.setString(1, unGranjero.getDni());
            ps.setString(2, unGranjero.getNombre());
            ps.setString(3, unGranjero.getApellidos());
            if (unGranjero.getFechaNac() == null)
                ps.setDate(4, null);
            else
                ps.setDate(4, Util.dateSQL(unGranjero.getFechaNac()));
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                unGranjero.setId(rs.getInt(1));
                break;
            }
            System.out.println("actualizado el granjero");
            updateTbCampos(unGranjero);;
            updateTbMaquinarias(unGranjero);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void modificarGranjero(Granjero elGranjero){
        String sentencia = UPDATE + TB_GRANJERO + "SET dni=?, nombre=?, apellidos=?, fecha_nac=? " +
                WHERE+" id=?";
        PreparedStatement ps;
        try {
            ps = db.prepareStatement(sentencia);
            ps.setString(1, elGranjero.getDni());
            ps.setString(2, elGranjero.getNombre());
            ps.setString(3, elGranjero.getApellidos());
            if (elGranjero.getFechaNac() == null)
                ps.setDate(4, null);
            else
                ps.setDate(4, Util.dateSQL(elGranjero.getFechaNac()));
            ps.setInt(5, elGranjero.getId());
            ps.executeUpdate();
            updateTbCampos(elGranjero);
            updateTbMaquinarias(elGranjero);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void updateTbCampos(Granjero elGranjero) {
        String sentencia = UPDATE + TB_CAMPO + "SET propietario = ? " +
                WHERE + "id = ?";
        PreparedStatement ps;
        try {
            for (int idCampos: elGranjero.getListaCampos()){
                ps = db.prepareStatement(sentencia);
                ps.setInt(1, elGranjero.getId());
                ps.setInt(2, idCampos);
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void updateTbMaquinarias(Granjero elGranjero) {
        String sentencia = UPDATE + TB_MAQUINARIA + "SET propietario = ? " +
                WHERE + "id = ?";
        PreparedStatement ps;
        try {
            for (int idMaquina: elGranjero.getListaMaquinas()){
                ps = db.prepareStatement(sentencia);
                ps.setInt(1, elGranjero.getId());
                ps.setInt(2, idMaquina);
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void eliminarGranjero(Granjero unGranjero) {
        String sentencia = DELETE + TB_GRANJERO + WHERE + "id = ?";
        PreparedStatement ps = null;
        try {
            ps = db.prepareStatement(sentencia);
            ps.setInt(1, unGranjero.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void registrarCampo(Campo campo) {
        String sentencia = INSERT + TB_CAMPO + "(referencia, nombre, superficie, fecha_compra) " +
                VALUES + "(?, ?, ?, ?)";
        PreparedStatement ps;
        try {
            ps = db.prepareStatement(sentencia);
            ps.setString(1, campo.getReferencia());
            ps.setString(2, campo.getNombre());
            ps.setDouble(3, campo.getSuperficie());
            if (campo.getFechaCompra() == null)
                ps.setDate(4, null);
            else
                ps.setDate(4, Util.dateSQL(campo.getFechaCompra()));
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void modificarCampo(Campo campo){
        String sentencia = UPDATE + TB_CAMPO + "SET referencia=?, nombre=?, superficie=?, fecha_compra=? " +
                WHERE + "id = ?";
        PreparedStatement ps;
        try {
            ps = db.prepareStatement(sentencia);
            ps.setString(1, campo.getReferencia());
            ps.setString(2, campo.getNombre());
            ps.setDouble(3, campo.getSuperficie());
            if (campo.getFechaCompra() == null)
                ps.setDate(4, null);
            else
                ps.setDate(4, Util.dateSQL(campo.getFechaCompra()));
            ps.setInt(5, campo.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void eliminarCampo(Campo campo) {
        String sentenceia = DELETE + TB_CAMPO + WHERE + "id = ?";
        PreparedStatement ps;
        try {
            ps = db.prepareStatement(sentenceia);
            ps.setInt(1, campo.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Collection<Campo> obtenerCampos() {
        String sentencia = SELECT +" * "+FROM + TB_CAMPO;
        PreparedStatement ps;
        ResultSet rs;
        Campo campo;
        ArrayList<Campo> campos = new ArrayList<>();
        try{
            ps = db.prepareStatement(sentencia);
            rs = ps.executeQuery();
            while (rs.next()){
                campo = new Campo();
                campo.setId(rs.getInt(1));
                campo.setReferencia(rs.getString(2));
                campo.setNombre(rs.getString(3));
                campo.setSuperficie(rs.getDouble(4));
                campo.setFechaCompra(rs.getDate(5));
                campo.setPropietario(rs.getInt(6));
                campos.add(campo);
            }
            return campos;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void registrarMaquinaria(Maquinaria maquina) {
        String sentencia = INSERT + TB_MAQUINARIA + "(matricula, marca, tipo_maquinaria, fecha_compra) "+
                VALUES + "(?, ?, ?, ?)";
        PreparedStatement ps;
        try{
            ps = db.prepareStatement(sentencia);
            ps.setString(1, maquina.getMatricula());
            ps.setString(2, maquina.getMarca());
            ps.setString(3, maquina.getTipoMaquinaria().toString());
            if(maquina.getFechaCompra() == null)
                ps.setDate(4, null);
            else
                ps.setDate(4, Util.dateSQL(maquina.getFechaCompra()));
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void modificarMaquina(Maquinaria maquina){
        String sentencia = UPDATE + TB_MAQUINARIA + "SET matricula=?, marca=?, tipo_maquinaria=?, fecha_compra=? " +
                WHERE + "id = ?";
        PreparedStatement ps;
        try {
            ps = db.prepareStatement(sentencia);
            ps.setString(1, maquina.getMatricula());
            ps.setString(2, maquina.getMarca());
            ps.setString(3, maquina.getTipoMaquinaria().toString());
            if(maquina.getFechaCompra() == null)
                ps.setDate(4, null);
            else
                ps.setDate(4, Util.dateSQL(maquina.getFechaCompra()));
            ps.setInt(5, maquina.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void eliminarMaquina(Maquinaria maquina){
        String sentenceia = DELETE + TB_MAQUINARIA + WHERE + "id = ?";
        PreparedStatement ps;
        try {
            ps = db.prepareStatement(sentenceia);
            ps.setInt(1, maquina.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Collection<Maquinaria> obtenerMaquinarias(){
        String sentencia = SELECT +" * "+FROM + TB_MAQUINARIA;
        PreparedStatement ps;
        ResultSet rs;
        Maquinaria maquina;
        ArrayList<Maquinaria> maquinarias = new ArrayList<>();
        try{
            ps = db.prepareStatement(sentencia);
            rs = ps.executeQuery();
            while (rs.next()){
                maquina = new Maquinaria();
                maquina.setId(rs.getInt(1));
                maquina.setMatricula(rs.getString(2));
                maquina.setMarca(rs.getString(3));
                maquina.setTipoMaquinaria(Util.parseTipoMaquina(rs.getString(4)));
                maquina.setFechaCompra(rs.getDate(5));
                maquina.setPropietario(rs.getInt(6));
                maquinarias.add(maquina);
            }
            return maquinarias;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getRutaFicheroDatos() {
        return rutaFicheroDatos;
    }

    public void setRutaFicheroDatos(String rutaFicheroDatos) {
        this.rutaFicheroDatos = rutaFicheroDatos;

    }

    public String getRutaFicheroInicio() {
        return rutaFicheroInicio;
    }

    public void guardarRutaActual() {
        Properties config = new Properties();
        try {
            config.load(new FileInputStream(rutaFicheroInicio));
            config.setProperty("rutaFicheroDatos", rutaFicheroDatos);
            config.store(new FileOutputStream(rutaFicheroInicio), "Fichero de configuracion");
        } catch (IOException e) {
            Util.warningCampoVacio("Error de lectura", "Error en la lectura del fichero: "+rutaFicheroInicio);
        }
    }
    public Granjero obtenerGranjero(int idGranjero) {
        for (Granjero granjero: obtenerGranjeros()){
            if (granjero.getId() == idGranjero){
                return granjero;
            }
        }
        return null;
    }

    public Campo obtenerCampo(int idCampo) {
        for (Campo campo:obtenerCampos()){
            if (campo.getId() == idCampo){
                return campo;
            }
        }
        return null;
    }

    public Maquinaria obtenerMaquinaria(int idMaquina) {
        for (Maquinaria maquinaria: obtenerMaquinarias()){
            if (maquinaria.getId() == idMaquina){
                return maquinaria;
            }
        }
        return null;
    }

    public void cambiarConfigConexion(String sgbd, String rutaBBDD, int puerto, String bbdd) {
        Properties config = new Properties();
        try {
            config.load(new FileInputStream(rutaFicheroInicio));
            config.setProperty(PROPS_SGBD, sgbd);
            config.setProperty(PROPS_RUTABBDD, rutaBBDD);
            config.setProperty(PROPS_PUERTO, String.valueOf(puerto));
            config.setProperty(PROPS_BBDD, bbdd);
            config.store(new FileOutputStream(rutaFicheroInicio), "Fichero de configuracion");
        } catch (FileNotFoundException e) {
            Util.warningCampoVacio("Fichero no encontrado", "El fichero "+rutaFicheroInicio + " no se ha podido encontrar.");
            e.printStackTrace();
        } catch (IOException e) {
            Util.warningCampoVacio("Error de lectura o escritura", "Error en la lectura o escritura del fichero: "+rutaFicheroInicio);
            e.printStackTrace();
        }
    }

    public Collection<Granjero> buscarGranjero(String texto) {
        String sentencia = SELECT + "* " + FROM + TB_GRANJERO + WHERE + "dni " +
                LIKE + "? " + OR + "nombre " + LIKE + "? " + OR + "apellidos " + LIKE + "? " +
                OR + "fecha_nac " + LIKE + "?";
        PreparedStatement ps;
        ResultSet rs;
        ArrayList<Granjero> lista = new ArrayList<>();
        Granjero granjero;
        try {
            ps = db.prepareStatement(sentencia);
            ps.setString(1, texto);
            ps.setString(2, texto);
            ps.setString(3, texto);
            ps.setString(4, texto);
            rs = ps.executeQuery();
            while (rs.next()){
                granjero = new Granjero();
                granjero.setId(rs.getInt(1));
                granjero.setDni(rs.getString(2));
                granjero.setNombre(rs.getString(3));
                granjero.setApellidos(rs.getString(4));
                granjero.setFechaNac(rs.getDate(5));
                lista.add(granjero);
            }
            return lista;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void borrarPropietario(String tabla, int idObjeto) {
        String sentencia = UPDATE + tabla + "SET propietario = ? "+
                WHERE + "id = ?";
        PreparedStatement ps;
        try {
            ps = db.prepareStatement(sentencia);
            ps.setInt(1, 0);
            ps.setInt(2, idObjeto);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Collection<Campo> buscarCampos(String texto) {
        String sentencia = SELECT + "* " + FROM + TB_CAMPO + WHERE + "referencia " + LIKE + "? " +
                OR + "nombre " + LIKE + "? " + OR + "superficie " + LIKE + "? " +
                OR + "fecha_compra " + LIKE + "?";
        PreparedStatement ps;
        ResultSet rs;
        ArrayList<Campo> lista = new ArrayList<>();
        Campo campo;
        try {
            ps = db.prepareStatement(sentencia);
            ps.setString(1, texto);
            ps.setString(2, texto);
            ps.setString(3, texto);
            ps.setString(4, texto);
            rs = ps.executeQuery();
            while (rs.next()){
                campo = new Campo();
                campo.setId(rs.getInt(1));
                campo.setReferencia(rs.getString(2));
                campo.setNombre(rs.getString(3));
                campo.setSuperficie(rs.getDouble(4));
                campo.setFechaCompra(rs.getDate(5));
                lista.add(campo);
            }
            return lista;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Collection<Maquinaria> buscarMaquinas(String texto) {
        String sentencia = SELECT + "* " + FROM + TB_MAQUINARIA + WHERE + "matricula " + LIKE + "? " +
                OR + "marca " + LIKE + "? " + OR + "tipo_maquinaria " + LIKE + "? " +
                OR + "fecha_compra " + LIKE + "?";
        PreparedStatement ps;
        ResultSet rs;
        ArrayList<Maquinaria> lista = new ArrayList<>();
        Maquinaria maquina;
        try {
            ps = db.prepareStatement(sentencia);
            ps.setString(1, texto);
            ps.setString(2, texto);
            ps.setString(3, texto);
            ps.setString(4, texto);
            rs = ps.executeQuery();
            while (rs.next()){
                maquina = new Maquinaria();
                maquina.setId(rs.getInt(1));
                maquina.setMatricula(rs.getString(2));
                maquina.setMarca(rs.getString(3));
                maquina.setTipoMaquinaria(Util.parseTipoMaquina(rs.getString(4)));
                maquina.setFechaCompra(rs.getDate(5));
                lista.add(maquina);
            }
            return lista;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}