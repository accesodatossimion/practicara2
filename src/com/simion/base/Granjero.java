package com.simion.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Clase que gestiona los datos referentes a un granjero.
 * Created by Simion on 31/10/2016.
 * @author Simion Bordean
 * @version 1.0 final
 */
public class Granjero{

    private int id;
    private String dni;
    private String nombre;
    private String apellidos;
    private Date fechaNac;
    private List<Integer> listaCampos;
    private List<Integer> listaMaquinas;

    /**
     * Contructor de la clase Granjero
     */
    public Granjero() {
        listaCampos = new ArrayList<>();
        listaMaquinas = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Metodo que obtiene el DNI de un granjero
     * @return el DNI de un granjero
     */
    public String getDni() {
        return dni;
    }

    /**
     * Metodo que modifica el DNI de un granjero
     * @param dni
     */
    public void setDni(String dni) {
        this.dni = dni;
    }

    /**
     * Metodo que obtiene el nombre del granjero
     * @return el nombre del granjero
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo que modifica el nobmre de un granjero
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo que obtiene los apellidos del granjero
     * @return los apellidos del granjero
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * Metodo que moodifica los apellidos del granjero
     * @param apellidos
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * Metodo que obtiene la fecha de nacimiento del granjer en formato Date
     * @return la fecha de nacimiento del granjero
     */
    public Date getFechaNac() {
        return fechaNac;
    }

    /**
     * Metodo que modifica la fecha de nacimiento del granjero
     * @param fechaNac
     */
    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    /**
     * Metodo que obtiene la lista de los campos de un granjero en formato de ArrayList
     * @return la lista de campos del granjero
     */
    public List<Integer> getListaCampos() {
        return listaCampos;
    }

    /**
     * Metodo que guarda la lista de campos del granjero en formatod de ArrayList
     * @param listaCampos
     */
    public void setListaCampos(List<Integer> listaCampos) {
        this.listaCampos = listaCampos;
    }

    /**
     * Metodo que obtiene la lista de maquinarias de un granjero en formato ArrayList
     * @return la lista de maquinarias de un granjero
     */
    public List<Integer> getListaMaquinas() {
        return listaMaquinas;
    }

    /**
     * Metodo que modifica la lista de maquinarias de un granjero
     * @param listaMaquinas
     */
    public void setListaMaquinas(List<Integer> listaMaquinas) {
        this.listaMaquinas = listaMaquinas;
    }

    /**
     * Metodo que devuelve por defecto el dni de un granjero al usar el objeto Granjero
     * @return el dni del granjero
     */
    @Override
    public String toString() {
        if(nombre.equals("") && apellidos.equals("")){
            return dni;
        }
        else
            return nombre + " " + apellidos;
    }
}