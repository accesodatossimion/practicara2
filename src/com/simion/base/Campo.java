package com.simion.base;

import java.util.Date;

/**
 * Clase que gestiona los datos referentes a un Campo
 * Created by Simion on 31/10/2016.
 * @author Simion Bordean
 * @Verision: 1.0 final
 */
public class Campo{

    private int id;
    private String referencia;
    private String nombre;
    private double superficie;
    private Date fechaCompra;
    private int propietario;

    /**
     * Constructor del la clase Campo
     */
    public Campo() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Metodo que obtiene la referencia de un campo.
     * @return Referencia del Campo.
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * Metodo que modifica la referencia de un Campo determinado.
     * @param referencia del Campo
     */
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    /**
     * Metodo que devuelve el nombre de un campo en fortato String.
     * @return String nombre del Campo
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo que modifica el nombre de un campo
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo que obtiene la superfice de un campo.
     * @return la superficie del campo
     */
    public double getSuperficie() {
        return superficie;
    }

    /**
     * Metodo que modifica la superficie de un campo
     * @param superficie
     */
    public void setSuperficie(double superficie) {
        this.superficie = superficie;
    }

    /**
     * Metodo que obtiene la fecha de compra de un campo
     * @return la fecha del campo
     */
    public Date getFechaCompra() {
        return fechaCompra;
    }

    /**
     * Metodo que modifica la fecha de compra de un campo
     * @param fechaCompra
     */
    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    /**
     * Metodo que optiene el dni del porpietario del campo
     * @return el dni del propietario del campo
     */
    public int getPropietario() {
        return propietario;
    }

    /**
     * Metodo que modifica el dni del propietario de un campo
     * @param propietario
     */
    public void setPropietario(int propietario) {
        this.propietario = propietario;
    }

    /**
     * Metodo por defecto que convierte a String la referencia del campo al usar el objeto Campo
     * @return la referencia del campo.
     */
    @Override
    public String toString() {
        if(nombre.equals("")){
            return referencia;
        }
        else
            return nombre;
    }
}