package com.simion.base;

import java.util.Date;

/**
 * Clase que gestiona los datos sobre las maquinarias de un granjero
 * Created by Simion on 31/10/2016.
 * @author Simion Bordean
 * @version 1.0 final
 */
public class Maquinaria{

    private int id;
    private String matricula;
    private String marca;
    private TipoMaquinaria tipoMaquinaria;
    private Date fechaCompra;
    private int propietario;

    /**
     * Contructor de la clase maquinaria
     */
    public Maquinaria() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Metodo que obtiene la matricula de una maquinaria
     * @return la matricula de la maquinaria
     */
    public String getMatricula() {
        return matricula;
    }

    /**
     * Metodo que modifical la matricula de una maquinaria
     * @param matricula el valor de la matricula a modificar
     */
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    /**
     * Metodo que obtiene la marca de una maquinaria
     * @return la marca de la maquinaria
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Metodo que modifica la marca de una maquinaria
     * @param marca valor de la marca a modificar.
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * Metodo que obtiene el tipo de maquinaria en formato TipoMaquinaria
     * @return el tipio de maquinaria
     * @see <a href=TipoMaquinaria.html>TipoMaquinaria</a>
     */
    public TipoMaquinaria getTipoMaquinaria() {
        return tipoMaquinaria;
    }

    /**
     * Metodo que modifica el tipo de maquinaria mediante el formato TipoMaquinaria
     * @param tipoMaquinaria el valor del objeto de tipo TipoMauqinaria a modifica
     * @see <a href=TipoMaquinaria.html>TipoMaquinaria</a>
     */
    public void setTipoMaquinaria(TipoMaquinaria tipoMaquinaria) {
        this.tipoMaquinaria = tipoMaquinaria;
    }

    /**
     * Metodo que obtiene la fecha de compra de la maquinaria.
     * @return la fecha de compra de la maquinaria
     * @see <a href=https://docs.oracle.com/javase/7/docs/api/java/util/Date.html>Date</a>
     */
    public Date getFechaCompra() {
        return fechaCompra;
    }

    /**
     * Metodo dado un valor de tipo Date modifica la fecha de la maquinaria
     * @param fechaCompra parametro a pasar al metodo
     * @see <a href=https://docs.oracle.com/javase/7/docs/api/java/util/Date.html>Date</a>
     */
    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    /**
     * Metodo que obtiene el propietario de la maquinaria
     * @return el propietario de la maquinaria
     */
    public int getPropietario() {
        return propietario;
    }

    /**
     * Metodo que modifica el propietario de una maquinaria
     * @param propietario parametro que modifica el propietario de una maquinaria
     */
    public void setPropietario(int propietario) {
        this.propietario = propietario;
    }

    /**
     * Metodo que por defecto devuelve la matricula de la maquinaria al usar un objeto de tipo Maquinaria
     * @return la matricula de la maquinaria
     */
    @Override
    public String toString() {
        if(marca.equals("")){
            return matricula;
        }
        else
            return marca;
    }
}