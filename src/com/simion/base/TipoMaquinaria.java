package com.simion.base;

/**
 * Clase de tipo enumerado que contiene los posibles tipos de maquinarias
 * Created by Simion on 31/10/2016.
 * @author Simion Bordean
 * @version 1.0 final
 */
public enum TipoMaquinaria {
    TRACTOR, COSECHADORA, ARADO, CULTIVADOR, SEMBRADORA, REMOLQUE, EMPACADORA, PALA;
}
