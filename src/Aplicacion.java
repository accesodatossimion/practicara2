import com.simion.gui.Controlador;
import com.simion.gui.Modelo;
import com.simion.gui.Ventana;

/**
 * Clase principal desde donde empieza el hilo de ejecucion.
 * Created by Simion on 30/10/2016.
 * @author Simion Bordean
 */
public class Aplicacion {

    /**
     * Metodo desde donde empieza la ejecucion del programa
     * @param args
     */
    public static void main (String[] args){


        Modelo modelo = new Modelo();
        Ventana view = new Ventana();
        Controlador controlador = new Controlador(modelo, view);
    }
}
